<?php

/*
 * Here I put some useful (for me) functions
 * Basically, saving thins in txt LOGs makes me esaier life on enviroments
 * with tons of messages in dblog.
 *
 * @TODO: THIS WILL be removed on RC
 *
 * */

// JavaScript Minifier
function _bpn_minify_js($input) {
  if(trim($input) === "") return $input;
  return preg_replace(
    array(
      // Remove comment(s)
      '#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#',
      // Remove white-space(s) outside the string and regex
      '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/)|\/(?!\/)[^\n\r]*?\/(?=[\s.,;]|[gimuy]|$))|\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#s',
      // Remove the last semicolon
      '#;+\}#',
      // Minify object attribute(s) except JSON attribute(s). From `{'foo':'bar'}` to `{foo:'bar'}`
      '#([\{,])([\'])(\d+|[a-z_][a-z0-9_]*)\2(?=\:)#i',
      // --ibid. From `foo['bar']` to `foo.bar`
      '#([a-z0-9_\)\]])\[([\'"])([a-z_][a-z0-9_]*)\2\]#i'
    ),
    array(
      '$1',
      '$1$2',
      '}',
      '$1$3',
      '$1.$3'
    ),
    $input);
}


/**
 * FORMATOS de Error
 * [DD/MM/YYYY HH24:MI:SS][PROCESO][SUBPROCESO][NIVEL] Texto
 * *
 * [DD/MM/YYYY HH24:MI:SS] =>  Timestamp, hasta segundos
 * [PROCESO]               =>  Proceso que escribe el log, si el log es
 * utilizado por más de un proceso
 * [SUBPROCESO]            =>  SubProceso, si lo hay
 * [NIVEL]                 =>  INFO, WARNING, ERROR, FATAL
 * [EXCEPTION]             =>  Descripción ampliada del mensaje
 */
function _bpn_write_log($process, $subprocess = 'sub-process', $level = 'Notice', $message) {

  $filename = $process . '.log';
  $text_to_log = '[' . date('H:i:s') . '][' . $process . '][' . $subprocess . '][' . $level . '][' . $message . ']' . "\n";

  _bpn_save_tofile($filename, $text_to_log);

}

/**
 * Writes to file the string in arguments
 * @param $filename
 * @param $text_to_log
 */
function _bpn_save_tofile($filename, $text_to_log) {

  $final_path = drupal_get_path('module', 'browser_push_notifications'). '/logs/' . date('Y-m-d') . '-' . $filename;
  try {
    $logfile = fopen($final_path, "a+");
  } catch (Exception $e) {
    throw $this->createNotFoundException('Can not open/create file: ' . $final_path);
  }

  fwrite($logfile, $text_to_log);
  fclose($logfile);

}
