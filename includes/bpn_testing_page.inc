<?php

function _bpn_test_current_settings() {

  global $base_url;
  drupal_add_library('system', 'ui.dialog');
  drupal_add_js(['bpn' => ['basepath' => $base_url]], 'setting');

  // We ensure it gets registered on all supported browsers by calling its URL
  //  drupal_add_js(['bpn' => ['worker' => $base_url . '/serviceWorker.js']], 'setting');
  drupal_add_js(drupal_get_path('module', 'browser_push_notifications') . '/js/bpn_tester.js');

  // If browser has enabled/avaliable subscriptions, we can offer two options:
  // pop_up => Offers POP-UP Window to user for initiate the process, or
  // direct_try => Which tries for direct subscription (default)

  // TODO: Add settings config to configure this options
  $activate_option = variable_get('bpn_subs_option', 'direct_try');
  drupal_add_js(['bpn' => ['activate_option' => $activate_option]], 'setting');

  $test_content = '<h1>Web Push Notifications Check</h1>';
  $test_content .= '<p>' . t('Here you can check enabling/disabling subscription on your browser, <br />show browser compatibility and <b>send a test notification to yourself</b> (if your browser supports it).') . '</p>';
  $test_content .= '<button id="push-subscription-button">Push notifications !</button>&nbsp;&nbsp;&nbsp;';
  $test_content .= '<button id="send-push-button">Send a push notification</button>';

  return $test_content;
}


function _bpn_send_test_notification() {

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $data = json_decode(file_get_contents('php://input'), TRUE);
    if (isset($data)) {
      if (!isset($data['token'])) {
        // If we do not have these values, may be is a bot trying to post, so...
        return drupal_not_found();
      }

      global $base_url;

      $subscription = new stdClass();
      $subscription->subscription_key = filter_xss($data['key']);
      $subscription->subscription_token = filter_xss($data['token']);
      $subscription->subscription_endpoint = filter_xss($data['endpoint']);

      $payload = [
        'bpn_title' => t('It WORKS !!'),
        'bpn_body' => t('Now, you can click on this notification to go to the "Send Notifications page"'),
        'bpn_icon' => $base_url . '/sites/all/modules/custom/browser_push_notifications/assets/bell.png',
        'bpn_url' => $base_url . '/admin/content/browser_push_notifications',
      ];

      $result = _bpn_send_this_notification($subscription, $payload);
    }
  }

  return drupal_json_output($result);

}