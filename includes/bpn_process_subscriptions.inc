<?php


/**
 * Get the input from bpn_tester.js subscription object posted to
 * /push_subscription URL
 *
 * @return string
 * @throws \InvalidMergeQueryException
 */
function _bpn_process_subscriptions() {

  // Only answer questions made over HTTP POST METHOD,
  // Because I do not know how to deal other METHODS like PUT in Drupal 7...
  // But we trick for other METHODS with --$data['drupal_action']-- inside...

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // Uncomment the following line for testing with POSTMAN
    // return drupal_json_output($_POST);

    $posted_input = file_get_contents('php://input');
    $data = json_decode($posted_input, TRUE);

    if (count($data) == 5) { // We DO EXPECT exactly 5 items

      if (!isset($data['drupal_action']) || !$data['subscription_token']) {
        // If we do not have these values, may be is a bot trying to post, so...
        return drupal_not_found();
      }

      if ($data['drupal_action'] == 'PUT' || $data['drupal_action'] == 'UPDATE') {
        $endpoint = filter_xss($data['endpoint']);
        $entry['subscription_endpoint'] = $endpoint;
        $entry['subscription_key'] = filter_xss($data['key']);
        $entry['subscription_token'] = filter_xss($data['subscription_token']);
        $entry['uid'] = filter_xss($data['nuid']);
        $entry['updated'] = strtotime(date('Y-m-d H:i:s'));

        $result = db_merge('browser_subscriptions')
          ->key(['subscription_endpoint' => $endpoint])
          ->fields($entry)
          ->execute();
        $result = (is_numeric($result)) ? 'success' : 'failure';

//        watchdog('Browser Push Notifications', 'Suscription registered/updated', $entry, WATCHDOG_NOTICE, $link = NULL);

        return drupal_json_output($result);

      }

      elseif ($data['drupal_action'] == 'DELETE') {

        $endpoint = filter_xss($data['endpoint']);
        $deleted = db_delete('browser_subscriptions')
          ->condition('subscription_endpoint', $endpoint)
          ->execute();
        $deleted = (is_numeric($deleted)) ? 'success' : 'failure';

        return drupal_json_output($deleted);

      }

      else {
        // If bots try to feed garbage in, we reject them here.
        return drupal_not_found();
      }
    }
  }

  // If NO HTTP POST method, nobody has anything to find here.
  return drupal_not_found();

}