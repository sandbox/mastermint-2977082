<?php

// @TODO: Make a custom entity to have record of sent notifications.
/**
 * Implements hook_form()
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function _bpn_send_notifications_form($form, &$form_state) {

  global $base_url;

  $form['intro_message'] = [
    '#markup' => t('<h1>Send Notifications to users</h1>'),
  ];
  //*********************************
  // Recipients SECTION
  $form['recipients'] = [
    '#type' => 'fieldset',
    '#title' => t('What users are going to receive the notification?'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['recipients']['roles'] = [
    '#type' => 'checkboxes',
    '#options' => user_roles(),
    '#title' => t('Users by role'),
  ];

  $form['recipients']['specific_users'] = [
    '#type' => 'textfield',
    '#title' => t('Specific user(s)'),
    '#multiple' => TRUE,
    '#autocomplete_path' => 'bpn_users/autocomplete',
    '#size' => '120',
    '#description' => 'A comma separated list of users (supports autocomplete)',
  ];
  //*********************************
  // Message SECTION
  $form['message_to_send'] = [
    '#type' => 'fieldset',
    '#title' => t('What do you wanna tell them?'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['message_to_send']['title'] = [
    '#type' => 'textfield',
    '#default_value' => t('Title of the notification (be short, please)'),
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 80,
    '#description' => t('Here you must fill the TITLE shown in the notification...'),
  ];
  $form['message_to_send']['body'] = [
    '#type' => 'textarea',
    '#default_value' => t('Write down here the body of the notification (be short, please)'),
    '#title' => t('Body'),
    '#size' => 60,
    '#maxlength' => 160,
    '#description' => t('Here you must fill the BODY shown in the notification...'),
  ];
  $form['message_to_send']['destination'] = [
    '#type' => 'textfield',
    '#default_value' => $base_url,
    '#title' => t('Destination'),
    '#size' => 60,
    '#maxlength' => 160,
    '#description' => t('Were browser should go on clicking the pushed notification?'),
  ];
  $form['message_to_send']['notification_icon'] = [
    '#type' => 'managed_file',
    '#name' => 'bpn_notification_icon',
    '#title' => t('ICON for the notification'),
    '#size' => 40,
    '#description' => t("Image should be 250x250 pixels and in PNG format. If you don't choose one, we'll use the default"),
    '#upload_location' => 'public://bpn',
  ];


  // Send button
  $form['send_notifications'] = [
    '#type' => 'submit',
    '#value' => 'Send Notifications',
  ];

  return $form;
}

function _bpn_send_notifications_form_validate($form, &$form_state) {

  $have_users = FALSE;
  foreach ($form_state['values']['roles'] as $rol) {
    if ($rol != 0) {
      $have_users = TRUE;
    }
  }
  if (strlen($form_state['values']['specific_users']) != 0) {
    $have_users = TRUE;
  }

  if (!$have_users) {
    form_set_error('recipients', 'You must choose some recipient');
  }

}

function _bpn_send_notifications_form_submit($form, &$form_state) {

  global $base_url;
  $founded_subscritions = FALSE;
  $values = $form_state['values'];
  $user_uids = [];

  foreach ($values['roles'] as $key => $rid) {

    // if not selected, continue...
    if ($rid == 0) {
      continue;
    }

    // If anonimous selected, set uid "0" for selects...
    if ($key == 1 && $rid == 1) {
      $user_uids[] = 0;
    }

    // Other way, we check for users in role.
    $result = db_query('SELECT users.uid
        FROM users_roles LEFT JOIN users ON users_roles.uid = users.uid
        WHERE users_roles.rid = :rid', [':rid' => $rid]);
    foreach ($result as $user) {
      $user_uids[] = $user->uid;
    }
  }

  // Now we go for specific users, manually selected...
  $specific_users = (strlen($values['specific_users']) > 0) ? explode(', ', $values['specific_users']) : FALSE;
  if ($specific_users) {
    foreach ($specific_users as $specific_user) {
      $user_uids[] = db_query('SELECT u.uid FROM {users} u WHERE u.name = (:names)', array(':names' => $specific_user))->fetchField();
    }
  }

  // As users can be in multiple roles, we "uniquefy" them...
  array_unique($user_uids);

  // For each user, we check for subscription to send the notifiction.
  if(count($user_uids) > 0) {

    $subscriptions_pack = [];

    foreach ($user_uids as $uid) {
      $subscriptions = db_select('browser_subscriptions', 'bs')
        ->fields('bs', [
          'id',
          'subscription_endpoint',
          'subscription_key',
          'subscription_token',
        ])
        ->condition('uid', $uid)
        ->execute();

      $num_of_results = $subscriptions->rowCount();
      if($num_of_results > 0) {
        $founded_subscritions = TRUE;
        foreach ($subscriptions as $suscription) {
          $subscriptions_pack[] = $suscription;
        }
      }
    }
  }



  if ($founded_subscritions) {
    // We prepare the required body/fields to build the notification.
    $title = $form_state['values']['title'];
    $body = $form_state['values']['body'];

    // ICON
    $fid = FALSE;
    $icon = $base_url . '/sites/all/modules/custom/browser_push_notifications/assets/bell.png';
    if (isset($form_state['values']['notification_icon'])
      && $form_state['values']['notification_icon'] != 0) {
      $file = file_load($form_state['values']['notification_icon']);
      $file->status = FILE_STATUS_PERMANENT;
      $result = file_save($file);
      $fid = $result->fid;
      $icon = file_create_url($result->uri);
    }

    $destination = $form_state['values']['destination'];
    // PAYLOAD
    $payload = [
      'bpn_title' => $title,
      'bpn_body' => $body,
      'bpn_icon' => $icon,
      'bpn_url' => $destination,
    ];

    // We pass the fid of image to delete the uploaded image at the end of the process.
      _bpn_send_push_notifications($subscriptions_pack, $payload, $fid);

  } else {
    // We can't find subscribed users to send a notification to.
    drupal_set_message(t('We can\'t find subscribed users to send a notification to.'));
  }

}


/**
 * Provides results for autolading users for user's field (in form above).
 *
 * @param string $string
 */
function _bpn_user_search($string = '') {
  $matches = [];
  if ($string) {
    $items = array_map('trim', explode(',', $string));
    $last_item = array_pop($items);
    $prefix = implode(', ', $items);

    $result = db_select('users')
      ->fields('users', ['name'])
      ->condition('name', '%' . db_like($last_item) . '%', 'LIKE')
      ->range(0, 10)
      ->execute();
    foreach ($result as $user) {
      if (!in_array($user->name, $items)) {
        $value = !empty($prefix) ? $prefix . ', ' . $user->name : $user->name;
        $matches[$value] = check_plain($value);
      }
    }
  }
  drupal_json_output($matches);
}