<?php
/**
 * General settings FORM
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function bpn_general_settings_form($form, &$form_state) {

  global $base_url;
  drupal_add_library('system', 'ui.dialog');
  drupal_add_js(drupal_get_path('module', 'browser_push_notifications') . '/js/test_pop_up.js');
  drupal_add_css(drupal_get_path('module', 'browser_push_notifications') . '/css/bpn_main.css');

  $form['pop_up_message'] = [
    '#prefix' => '<div id="wrapper-test-dialog">',
    '#markup' => t('Test message to pop-up.'),
    '#suffix' => '</div>',
  ];

  $form['pop_up_message'] = [
    '#prefix' => '<div id="wrapper-test-dialog">',
    '#markup' => t('Test message to pop-up.'),
    '#suffix' => '</div>',
  ];

  $form['engagement_method'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Type of offer for visitors to subscribe'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  ];

  # the options to display in our form radio buttons
  $options = [
    'direct_try' => t('Direct try to subscribe. In that case, you can choose below the delay needed to spend before the event'),
    'pop_up'     => t('Via configurable POP-Up. This pop-up will show to the user <i>only</i> if user\'s browser supports Wep Push Notifications protocol'),
  ];

  $default_subs_method                      = variable_get('bpn_subs_option', 'pop_up');
  $form['engagement_method']['subs_method'] = [
    '#type'          => 'radios',
    '#title'         => t('How this module can try to subscribe visiting users?'),
    '#options'       => $options,
    '#default_value' => $default_subs_method,
  ];

  $default_body_message                                 = variable_get('bpn_subs_option_body', 'TEST');
  $form['engagement_method']['message_for_pop_up_body'] = [
    '#type'          => 'textfield',
    '#title'         => t('Body of the message'),
    '#default_value' => $default_body_message,
    '#size'          => 100,
    '#maxlength'     => 140,
    '#attributes'    => [
      'id' => 'test-body-pop-up',
    ],
    '#description'   => t('Take a look at the pop-up window we\'ll show to users for subscribe offers: <a id="test-pop-up" href="@open-me">Open pop-up</a>.', [
      '@open-me' => '#',
    ]),
  ];

  $default_time_to_load                                 = variable_get('bpn_subs_option_delay', 2000);
  $form['engagement_method']['time_lo_toad']            = [
    '#type'          => 'textfield',
    '#default_value' => $default_time_to_load,
    '#title'         => t('Wait "n" miliseconds to try the registration'),
    '#size'          => 6,
    '#maxlength'     => 5,
    '#description'   => t('For usability reasons, don\'t choose very high values...'),
  ];


  // Fieldset for RULES
  $form['rules_integration'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Rules Integration'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  ];

  if (module_exists('rules')) {
    $form['rules_integration']['message'] = [
      '#prefix' => '<h4>',
      '#markup' => t('Rules module detected, you are able to send notifications as reactions at rules events.'),
      '#suffix' => '</h4>',
    ];
  }
  else {
    $form['rules_integration']['message'] = [
      '#prefix' => '<h4>',
      '#markup' => t('Rules module not detected, if you install/enable it, you\'ll be able to send notifications as reactions at rules events.'),
      '#suffix' => '</h4>',
    ];
  }

  $default_subject        = variable_get('bpn_push_notifications_subject', $base_url);
  $form['bpn_sw_subject'] = [
    '#type'          => 'textfield',
    '#default_value' => $default_subject,
    '#title'         => t('ServiceWorker callback\'s Subject'),
    '#size'          => 100,
    '#maxlength'     => 140,
    '#description'   => t('Here you can change default value for ServiceWorker "Subject" and use a VALID EMAIL. This is needed for API Providers like Google'),
  ];

  // Send button
  $form['save_settings'] = [
    '#type'  => 'submit',
    '#value' => 'Save settings',
  ];

  return $form;
}

/**
 * Submit form
 * @param $form
 * @param $form_state
 */
function bpn_general_settings_form_submit($form, &$form_state) {

  variable_set('bpn_subs_option', $form_state['values']['subs_method']);
  variable_set('bpn_subs_option_body', $form_state['values']['message_for_pop_up_body']);
  variable_set('bpn_subs_option_delay', $form_state['values']['time_lo_toad']);
  variable_set('bpn_push_notifications_subject', $form_state['values']['bpn_sw_subject']);

}