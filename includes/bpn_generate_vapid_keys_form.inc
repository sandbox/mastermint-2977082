<?php

function _bpn_generate_vapid_keys_form($form, &$form_state) {

  $setted_public_key = variable_get('bpn_push_notifications_public_key', NULL);
  $form['bpn_push_notifications_public_key'] = [
    '#type' => 'textfield',
    '#default_value' => $setted_public_key,
    '#title' => t('Public Key'),
    '#size' => 100,
    '#maxlength' => 140,
    '#prefix' => '<div id="change-bpn-public-key">',
    '#suffix' => '</div>',
    '#description' => t('Here you can put your public key generated at <a href="https://web-push-codelab.glitch.me/" target="_blank">Push Companion</a> or use "Generate Keys" button below'),
  ];
  if ($setted_public_key !== NULL) {
    $form['bpn_push_notifications_public_key']['#disabled'] = TRUE;
  }

  $setted_private_key = variable_get('bpn_push_notifications_private_key', NULL);
  $form['bpn_push_notifications_private_key'] = [
    '#type' => 'textfield',
    '#default_value' => $setted_private_key,
    '#title' => t('Private Key'),
    '#size' => 46,
    '#maxlength' => 100,
    '#prefix' => '<div id="change-bpn-private-key">',
    '#suffix' => '</div>',
    '#description' => t('Here you can put your private key generated at <a href="https://web-push-codelab.glitch.me/" target="_blank">Push Companion</a> or use "Generate Keys" button below'),
  ];
  if ($setted_private_key !== NULL) {
    $form['bpn_push_notifications_private_key']['#disabled'] = TRUE;
  }


  if ($setted_public_key === NULL && $setted_private_key === NULL) {
    $form['generate_keys'] = [
      '#type' => 'button',
      '#value' => t('Generate keys'),
      '#href' => '',
      '#ajax' => [
        'callback' => '_bpn_once_key_generation',
      ],
    ];

    $form['save_keys'] = [
      '#type' => 'submit',
      '#value' => 'Save keys',
      '#weight' => 10,
    ];
  }
  else {

    $form['reset_aki_keys'] = [
      '#type' => 'fieldset',
      '#title' => t('Reset all Keys'),
      '#weight' => 9,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $form['reset_aki_keys']['contact_information'] = [
      '#prefix' => '<h2>',
      '#markup' => t('Be careful. Resetting keys will invalidate ALL current subcriptions you may have !!!'),
      '#suffix' => '</h2>',
    ];

    $form['reset_aki_keys']['reset_keys'] = [
      '#type' => 'submit',
      '#value' => 'reset keys, anyway',
      '#weight' => 7,
    ];
  }

  return $form;
}


/**
 * Save API keys or
 * reset API keys
 *
 * @param $form
 * @param $form_state
 */
function _bpn_generate_vapid_keys_form_submit($form, &$form_state) {

  if (isset($form_state['values']['save_keys'])) {
    variable_set('bpn_push_notifications_public_key', $form_state['values']['bpn_push_notifications_public_key']);
    variable_set('bpn_push_notifications_private_key', $form_state['values']['bpn_push_notifications_private_key']);
  }

  if (isset($form_state['values']['reset_keys'])) {
    variable_del('bpn_push_notifications_public_key');
    variable_del('bpn_push_notifications_private_key');
  }
}

/**
 * Callback for AJAX at $form['generate_keys'] item.
 * @param $form
 * @param $form_state
 *
 * @return array
 */
function _bpn_once_key_generation($form, &$form_state) {

  $vapid = new Minishlink\WebPush\VAPID();
  $keys = $vapid::createVapidKeys();

  $form['bpn_push_notifications_public_key']['#value'] = $keys['publicKey'];
  $form['bpn_push_notifications_private_key']['#value'] = $keys['privateKey'];

  $commands = [];
  $commands[] = ajax_command_replace('#change-bpn-public-key', render($form['bpn_push_notifications_public_key']));
  $commands[] = ajax_command_replace('#change-bpn-private-key', render($form['bpn_push_notifications_private_key']));

  return ['#type' => 'ajax', '#commands' => $commands];

}