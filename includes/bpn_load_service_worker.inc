<?php
/**
 * @file
 */

/**
 * Deliver the JS for the service worker.
 *
 * Adds a Service-Worker-Allowed header so that a file served from
 * 'bpn/bpn_serviceworker.js' can have a scope of '/'.
 */
function bpn_deliver_js_file($page_callback_result) {
  drupal_add_http_header('Content-Type', 'application/javascript');
  drupal_add_http_header('Service-Worker-Allowed', base_path());
  print $page_callback_result;
}

/**
 * Returns the JS of the service worker.
 *
 * @return mixed
 */
function bpn_serviceworker_file_data($version = 1) {

  $path = drupal_get_path('module', 'browser_push_notifications');
  $data = file_get_contents($path . '/js/serviceworker_source.min.js');

  return $data;
}