<?php

/**
 * @file
 * Contains browser_push_notifications.install functionality.
 */

/**
 * Implements hook_requirements().
 *
 * @param $phase
 *
 * @return array
 */
function browser_push_notifications_requirements($phase) {

  $requirements = [];
  $t            = get_t();

  if ($phase == 'runtime') {

    // Check PHP. Min version to work => 7.1.0
    $version = explode('.', PHP_VERSION);
    define('PHP_VERSION_BPN', ((int) $version[0] * 100 + (int) $version[1] * 10 + (int) $version[2]));

    if (PHP_VERSION_BPN < 710) {
      $requirements['gulp-php-min-version'] = [
        'title'       => 'Browser Push Notifications - Minimun PHP version for library "web-token/jwt-signature"',
        'value'       => 'php >= 7.1',
        'severity'    => REQUIREMENT_ERROR,
        'description' => $t('Gulp library will need PHP to be more than 7.1.x to work'),
      ];
    }

    if (!extension_loaded('gmp')) {
      $requirements['gulp-php-gmp'] = [
        'title'       => 'Browser Push Notifications - PHP extension not installed/loaded. Required by "web-token/jwt-key-mgmt"',
        'value'       => 'php >= 7.1',
        'severity'    => REQUIREMENT_ERROR,
        'description' => $t('Browser Push Notifications requires !web_push_gmp, which is missing.', [
          '!web_push_gmp' => l($t('GMP extension (PHP Server library)'), 'http://php.net/manual/en/book.gmp.php'),
        ]),
      ];
    }

    if (!extension_loaded('curl')) {
      $requirements['gulp-php-curl'] = [
        'title'       => 'Browser Push Notifications - curl PHP extension extension not installed/loaded but is required',
        'value'       => 'php >= 7.1',
        'severity'    => REQUIREMENT_ERROR,
        'description' => $t('Browser Push Notifications requires !web_push_curl, which is missing.', [
          '!web_push_curl' => l($t('cUrl extension (PHP Server library)'), 'http://php.net/manual/en/book.curl.php'),
        ]),
      ];
    }

    if (function_exists('libraries_get_path')) {
      $web_push_path = libraries_get_path('web-push-php');
      if (!$web_push_path) {
        $requirements['web-push-php'] = [
          'title'       => 'Browser Push Notifications - Missing web-push-library',
          'value'       => 'web-push-php',
          'severity'    => REQUIREMENT_ERROR,
          'description' => $t('Browser Push Notifications requires !web_push, which is missing. Clone or download/extract the entire contents of the archive into the %path directory on your server and execute composer install. Read the module\'s README.txt', [
            '!web_push' => l($t('web-push-php Library'), 'https://github.com/web-push-libs/web-push-php.git'),
            '%path'     => 'sites/all/libraries/web-push-php',
          ]),
        ];
      }
    }

    // Check we are ON HTTPS
    if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') || (isset($_SERVER["REQUEST_SCHEME"]) && $_SERVER["REQUEST_SCHEME"] === 'https')) {
      $requirements['bpn'] = array(
        'title' => $t('Browser Push Notifications'),
        'value' => $t('HTTPS on'),
        'severity' => REQUIREMENT_OK,
        'description' => $t('Please make sure the certificate of %domain is valid for Browser Push Notifications to work.', ['%domain' => $_SERVER['HTTP_HOST']]),
      );
    }
    elseif (in_array($_SERVER['HTTP_HOST'], ['localhost', '127.0.0.1'])) {
      $requirements['bpn'] = array(
        'title' => $t('Browser Push Notifications'),
        'value' => 'localhost',
        'severity' => REQUIREMENT_WARNING,
        'description' => $t('You will need to configure HTTPS on your production site for the Browser Push Notifications to function.'),
      );
    }
    else {
      $requirements['bpn'] = array(
        'title' => $t('Browser Push Notifications'),
        'value' => $t('HTTPS off'),
        'severity' => REQUIREMENT_ERROR,
        'description' => $t('HTTPS needs to be configured to enable your Browser Push Notifications. Without a secure connection, the Service Worker will not install itself.'),
      );
    }
  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function browser_push_notifications_schema() {
  $schema['browser_subscriptions'] = [
    'description' => 'The base table for storing browser subscription details',
    'fields'      => [
      'id'                    => [
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
        'length'      => 12,
        'description' => "The id for the table that is autoincremental id.",
      ],
      'subscription_endpoint' => [
        'description' => 'Stores browser subscription endpoint.',
        'type'        => 'text',
        'size'        => 'normal',
        'not null'    => TRUE,
      ],
      'subscription_key'      => [
        'description' => 'Store crypto key.',
        'type'        => 'text',
        'size'        => 'normal',
        'not null'    => TRUE,
      ],
      'subscription_token'    => [
        'description' => 'Store authorization details.',
        'type'        => 'text',
        'size'        => 'normal',
        'not null'    => TRUE,
      ],
      'uid'                   => [
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'description' => "User's ID (uid)",
      ],
      'updated'         => [
        'description' => 'Time when an subscription is registered/updated.',
        'type'        => 'int',
        'size'        => 'normal',
        'not null'    => TRUE,
      ],
    ],
    'primary key' => ['id'],
  ];
  return $schema;
}


/**
 * Implements hook_install().
 */
function browser_push_notifications_install() {
  $directory = file_default_scheme() . '://bpn';
  file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
}

/**
 * Implements hook_uninstall().
 */
function browser_push_notifications_uninstall() {
  // Delete remaining general module variables.
  db_delete('variable')
    ->condition('name', 'bpn_%', 'LIKE')
    ->execute();
}