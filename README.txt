REV:
https://www.reinisfischer.com/how-setup-web-push-notifications-drupal-8-or-any-other-site-using-onesignal
Free Service

This is a D7 version from the D8 module used as skeleton.
In this version I added basic functionality I've missed in D8 module,
like changing title on each message, or using custom icons on each message, or making message link to custom page on our site.
This module works queueing messages (drupal batch API) or delegating to drush for better performance on big sites.
This module provides an API to be used by other modules to send notifications.

Instructions/Install:
Clone into libraries folder, commonly at sites/all/libraries
https://github.com/web-push-libs/web-push-php.git

It will create a "web-push-php" folder.
cd into that folder and execute "composer install" (you must have composer installed)
Wait until finished.


Here you can read some TIPS for PERFORMANCE on regular use of this module:
https://www.kollegorna.se/en/2017/06/service-worker-gotchas/#service-worker-is-a-part-of-progressive-web-apps

For development:
Build certificate here:
https://certificatetools.com/newui/
or deploy your OWN for LOCALs here (example for UBUNTU servers):
https://fabianlee.org/2018/02/17/ubuntu-creating-a-trusted-ca-and-san-certificate-using-openssl-on-ubuntu/