Bell icon is used under CC license and it source is:
https://use.fontawesome.com/releases/v5.0.9/svgs/regular/bell.svg

From Font-Awesome, regarding the following:

Before you use:
This icon is licensed under the Creative Commons Attribution 4.0 International license
and requires that you comply with the following:

You must give appropriate credit, provide a link to the license,
=>  https://fontawesome.com/license
and indicate if changes were made.
You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.