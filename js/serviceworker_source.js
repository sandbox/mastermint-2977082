// Pop up the pushed notification
self.addEventListener('push', function(event) {
    var data = event.data ? event.data.text() : null;
    var received = JSON.parse(data);
    event.waitUntil(self.registration.showNotification(received.bpn_title, {
        body: received.bpn_body,
        icon: received.bpn_icon,
        badge: received.bpn_icon,
        tag: received.bpn_url
    }));
});
// Add Call-back for URL on Click
self.addEventListener('notificationclick', function(event) {
    event.notification.close();
    event.waitUntil(
        clients.openWindow(event.notification.tag)
    );
});