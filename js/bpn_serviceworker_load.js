(function ($, window, Drupal, navigator) {

    'use strict';

    if (!('serviceWorker' in navigator)) {
        return;
    }

    if (Drupal.settings.bpn.service_worker_system_status !== 'ready') {
        console.error('BPN Not ready: VAPID Keys not configured properly...');
        return;
    }

    const urlServerCall = Drupal.settings.bpn.service_worker_full_path;
    const applicationServerKey = Drupal.settings.bpn.serial;
    const nuid = Drupal.settings.bpn.service_worker_nuid;
    const offer = Drupal.settings.bpn.service_worker_offer;
    const message = Drupal.settings.bpn.service_worker_message;
    const timeToWait = Drupal.settings.bpn.service_worker_wait;
    var dialog_box =  $("#wrapper-bpn-dialog");

    console.log(urlServerCall);

    navigator.serviceWorker.register(urlServerCall, {scope: '/'})
        .then(() => {
        // console.log('[SW] Service worker has been registered [LOADED BPP_ServiceWorker_LOAD.js]');
        }, e => {
        console.error('[SW] Service worker registration failed', e);
    });

    if (!('serviceWorker' in navigator)) {
        console.warn("Service workers are not supported by this browser");
        return;
    }

    if (!('PushManager' in window)) {
        console.warn('Push notifications are not supported by this browser');
        return;
    }

    if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
        console.warn('Notifications are not supported by this browser');
        return;
    }

    // Coding data
    function urlBase64ToUint8Array(base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/');

        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);

        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    };

    // The subscriber function
    function push_subscribe() {
        navigator.serviceWorker.ready
            .then(
                serviceWorkerRegistration => serviceWorkerRegistration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: urlBase64ToUint8Array(applicationServerKey),
        })
        ).then(subscription => {
            // Subscription was successful
            // create subscription on your server
            return push_sendSubscriptionToServer(subscription, 'POST', 'PUT');
        }).then(subscription => subscription)
        .catch(e => {
            if(Notification.permission === 'denied')
            {
            // The user denied the notification permission which
            // means we failed to subscribe and the user will need
            // to manually change the notification permission to
            // subscribe to push messages
            console.warn('Notifications are denied by the user.');
            }
        else
            {
            // A problem occurred with the subscription; common reasons
            // include network errors or the user skipped the permission
            console.error('Impossible to subscribe to push notifications', e);
            }
        });
    };

    function push_sendSubscriptionToServer(subscription, method, drupal_method) {
        const key = subscription.getKey('p256dh');
        const token = subscription.getKey('auth');
        return fetch('/push_subscription', {
            method, // We use ONLY post as HTTP method, as D7 doesn't provide any standar way to deal with other ones.
            body: JSON.stringify({
                endpoint: subscription.endpoint,
                key: key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : null,
                subscription_token: token ? btoa(String.fromCharCode.apply(null, new Uint8Array(token))) : null,
                drupal_action: drupal_method,
                nuid: nuid
            }),
        })
        // .then(function (data) { console.log('Request succeeded with response data: ', data)})
            .then(() => subscription );
    };


    // UPDATE (on revisiting page)
    // We WAIT for LOADED page to register Workers, in order to
    // avoid breaking processes on low powered mobiles.
    // As recomended for progressive-web-apps
    window.addEventListener('load', function() {
        navigator.serviceWorker.register(urlServerCall, {scope: '/'})
            .then(() => {
            // console.log('[SW] Service worker has been updated');
        push_updateSubscription();
    }, e => {
            // console.error('[SW] Service worker update failed', e);
        });
    });

    function push_updateSubscription() {
        navigator.serviceWorker.ready
            .then(serviceWorkerRegistration => serviceWorkerRegistration.pushManager.getSubscription())
    .then(subscription => {
        if (!subscription) {
            // We aren't subscribed to push, so set UI to allow the user to enable push
            tryToSubscribeUser();
            return;
        };

        // Keep your server in sync with the latest endpoint
        return push_sendSubscriptionToServer(subscription, 'POST', 'UPDATE');
    })
    .then(subscription => subscription) // Set your UI to show they have subscribed for push messages
    .catch(e => {
            // console.error('Error when updating the subscription', e);
    });
    };

    function tryToSubscribeUser() {
        if(offer == 'direct_try') {
            setTimeout(function () {
                push_subscribe();
            }, timeToWait);

        } else {
            var text = '<h4 class="wrapper-body-message-pop-up">' + message + '</h4>',
                allow_bttn = '<button id="enable-push-subscription-button">Enable Push notifications</button>',
                cancel_bttn = '<button id="cancel-push-subscription-button">Cancel</button>',
                buttons = '<div class="buttons-wrapper">' + allow_bttn + '&nbsp;' + cancel_bttn + '</div>';
            dialog_box.html(text + buttons);

            setTimeout(function () {
                //
                dialog_box.dialog(
                    {
                        width: 380,
                        height: 200,
                        open: function (event, ui) {
                            // console.log('open: function [dialog callback]');
                            // $("#dialog-modal").html('Relleno con otra cosa');
                        }
                    });
                $('#enable-push-subscription-button').click(function (e) {
                    push_subscribe();
                    dialog_box.dialog( "close" );
                });
                $('#cancel-push-subscription-button').click(function (e) {
                    dialog_box.dialog( "close" );
                });
                }, timeToWait);

        };
    };


    /*
    // In case you want to unregister the SW during testing:
    navigator.serviceWorker.getRegistration()
      .then(function(registration) {
        registration.unregister();
      });
    /**/

}(jQuery, window, Drupal, navigator));
