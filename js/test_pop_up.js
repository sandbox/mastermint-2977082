(function ($, window, Drupal) {

    'use strict';
    Drupal.behaviors.browser_push_notifications = {
        attach: function (context, settings) {

            $('#test-pop-up').click(function (e) {
                e.preventDefault();
                var text = '<h4 class="wrapper-body-message-pop-up">' + $('#test-body-pop-up').val() + '</h4>',
                    allow_bttn = '<button id="enable-push-subscription-button">Enable Push notifications !</button>',
                    cancel_bttn = '<button id="cancel-push-subscription-button">Cancel</button>';
                $("#wrapper-test-dialog").html(text + '<br>' + allow_bttn + '&nbsp;' + cancel_bttn);
                $("#wrapper-test-dialog").dialog(
                    {
                        width: 300,
                        height: 180,
                        open: function (event, ui) {
                            // console.log('open: function [dialog callback]');
                            // $("#dialog-modal").html('Relleno con otra cosa');
                        }
                    });

                $('#cancel-push-subscription-button').click(function (e) {
                    console.log('CANCEL Clicked');
                    $("#wrapper-test-dialog").dialog( "close" );
                });
            });
        }
    };

})(jQuery, window, Drupal);